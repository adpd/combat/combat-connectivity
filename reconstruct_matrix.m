function A = reconstruct_matrix(lower_triangle)
% RECONSTRUCT_MATRIX reconstructs a square matrix with zeros on the diagonal
% from its lower triangle in column-major order.
%
% Inputs:
%   lower_triangle: a row vector containing the non-zero elements in the
%   lower triangle of the matrix, listed in column-major order.
%
% Outputs:
%   A: the square matrix with zeros on the diagonal.
    
    n = round(sqrt(length(lower_triangle) * 2)) + 1; % calculate the size of the square matrix
    A = zeros(n); % initialize the square matrix with zeros on the diagonal
    A(tril(true(n), -1)) = lower_triangle; % assign the lower triangle values to the matrix
    A = A + A.'; % fill in the upper triangle values by symmetry
end