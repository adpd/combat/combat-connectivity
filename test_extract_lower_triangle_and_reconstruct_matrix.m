% Define the test input
A = [0 1 2 3; 1 0 4 5; 2 4 0 6; 3 5 6 0];

% Extract the lower triangle in column-major order
lower_triangle = extract_lower_triangle(A);

% Reconstruct the matrix from the lower triangle
A_reconstructed = reconstruct_matrix(lower_triangle);

% Verify that the reconstructed matrix matches the original matrix
assert(isequal(A, A_reconstructed), 'Test failed: the reconstructed matrix does not match the original matrix.');
