function lower_triangle = extract_lower_triangle(A)
% EXTRACT_LOWER_TRIANGLE extracts the lower triangle of a square matrix in
% column-major order
%
% Inputs:
%   A: a zero-diagonal square matrix
%
% Outputs:
%   lower_triangle: a row vector containing the non-zero elements in the
%   lower triangle of the A matrix, listed in column-major order.

    n = size(A, 1);  % get the size of the square matrix
    lower_triangle = A(tril(true(n), -1)).';  % extract the lower triangle and transpose
    lower_triangle = lower_triangle(:).';  % transpose result to a row vector
end
